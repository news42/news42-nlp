import spacy
from spacy import displacy

if __name__ == "__main__":
    nlp = spacy.load("en")
    doc = nlp("At War: Afghan War Casualty Report: Nov. 16-22")
    p = 'linear-gradient(90deg, #aa9cfc, #fc9ce7)'
    n = 'linear-gradient(90deg, #8360c3, #2ebf91)'
    print(displacy.render(doc, style='ent', options={'bg': '#512DA8', 'colors': {'EVENT': p, 'DATE': n}}))
