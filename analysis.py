from textblob import TextBlob


class Analysis:
    def __init__(self, text):
        self.text = text
        from textblob.np_extractors import ConllExtractor
        from textblob.en.sentiments import NaiveBayesAnalyzer
        self.blob = TextBlob(text, np_extractor=ConllExtractor(), analyzer=NaiveBayesAnalyzer())

    def emotionality(self):
        a = [abs(x.sentiment.polarity) for x in self.blob.sentences]
        return sum(a) / len(a)

    def evaluation(self):
        return ((1 - self.emotionality()) + (2 * (1 - self.blob.sentiment.subjectivity))) / 3

    def score(self):
        return round(self.evaluation() * 1000)

    def get_topic(self):
        return self.blob.noun_phrases

    def is_topic(self):
        pass
