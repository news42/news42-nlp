import time
from pathlib import Path

import spacy
from bravado.client import SwaggerClient
from bravado.requests_client import RequestsClient
from spacy import displacy

from analysis import Analysis

if __name__ == '__main__':
    http_client = RequestsClient()
    http_client.set_api_key(
        'localhost', 'Token d1ecda554541092d6e1b69a21733a7ef88f648cc',
        param_name='Authorization', param_in='header',
    )
    client = SwaggerClient.from_url(
        'http://localhost:8000/swagger.json',
        http_client=http_client,
    )
    articles = client.rawarticles.rawarticles_list().response().result
    nlp = spacy.load('en')
    for article in articles:
        doc = nlp(article.text)
        sentence_spans = list(doc.sents)
        svg = displacy.render(sentence_spans, style='ent', options={'bg': '#512DA8', 'color': '#FFFFFF'})
